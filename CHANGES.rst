Changes history
===============

0.1.36 (2014-06-02)
------------------
Added setup.py file, some minor fixes, remove logger middleware.
